build: src/VectorAddition.java
	@mkdir -p out
	javac -d out src/VectorAddition.java

.PHONY: results.png
out/results.png:
	@mkdir -p out
	gnuplot -e "set autoscale;\
				set term pngcairo size 1920,1080;\
				set xlabel 'Threads';\
				set ylabel 'Frequency (Hz)';\
				set xtics 1;\
				set ytics 0.25;\
				set grid xtics lt 1 lw 1 lc rgb '#bbbbbb';\
				set grid ytics lt 1 lw 1 lc rgb '#bbbbbb';\
				set title 'Results';\
				set output 'out/results.png';\
				min(x, y) = (x < y ? x : y);\
				plot 'data/results.tsv' using 1:2   title 'Frequency vs Threads' with line,\
					 ''            using 1:2:3 title 'Standard Deviation'   with yerrorbars,\
					 min(x, 48) * 0.319091      title 'Expected Frequency vs Threads' with line\
				"            


out/timings.%.png: data/timings.%.tsv
	@mkdir -p out
	python2 src/gannt1.py -o $(patsubst %.png, %.gnuplot, $@) $<
	gnuplot -e "set term pngcairo size 1920,1080;\
				 set output '$@';\
				"\
			-c $(patsubst %.png, %.gnuplot, $@)

TIMING_DATA:=$(wildcard data/timings.*.tsv)
TIMING_PNGS:=$(patsubst data/%, out/%, $(patsubst %.tsv, %.png, $(TIMING_DATA)))

doc: src/report.Rmd out/results.png $(TIMING_PNGS)
	@mkdir -p out
	Rscript -e 'rmarkdown::render("src/report.Rmd")'
	mv src/report.pdf out/

clean:
	rm -rf out
