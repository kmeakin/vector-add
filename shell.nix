{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    # keep this line if you use bash
    bashInteractive

    (rWrapper.override { packages = with rPackages; [ rmarkdown ]; })
    pandoc
    texlive.combined.scheme-full
    gnuplot
    python2
  ];
}
