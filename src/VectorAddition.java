import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

class Timing implements Comparable<Timing> {
  int id;
  long start;
  long end;

  public Timing(int id, long start, long end) {
    this.id = id;
    this.start = start;
    this.end = end;
  }

  public int compareTo(Timing other) {
    if (this.start < other.start) {
      return -1;
    } else if (this.start > other.start) {
      return +1;
    } else {
      return 0;
    }
  }
}

class Adder implements Runnable {
  int id;
  Timing[] timings;

  int[] vec1;
  int[] vec2;
  int[] result;
  int start_idx;
  int end_idx;

  Adder(int id, Timing[] timings, int[] vec1, int[] vec2, int[] result, int start_idx, int end_idx) {
    this.id = id;
    this.timings = timings;

    this.vec1 = vec1;
    this.vec2 = vec2;
    this.result = result;
    this.start_idx = start_idx;
    this.end_idx = end_idx;
  }

  public void run() {
    long start = System.nanoTime();

    for (int i = this.start_idx; i < this.end_idx; i++) {
      for (int j = 0; j < 50; j++) {
        this.result[i] = this.vec1[i] + this.vec2[i];
      }
    }

    long end = System.nanoTime();
    this.timings[this.id] = new Timing(this.id, start, end);
  }
}

class Args {
  boolean benchmark;
  int num_samples;
  int num_threads;
  int length;

  public Args(boolean benchmark, int num_samples, int num_threads, int length) {
    this.benchmark = benchmark;
    this.num_samples = num_samples;
    this.num_threads = num_threads;
    this.length = length;
  }
}

class Result {
  long time_nanoseconds;
  double freq_hz;
  Timing[] timings;

  public Result(long time_nanoseconds, double freq_hz, Timing[] timings) {
    this.time_nanoseconds = time_nanoseconds;
    this.freq_hz = freq_hz;
    this.timings = timings;
  }
}

public class VectorAddition {
  static String array_to_string(int[] array) {
    StringBuilder sb = new StringBuilder("[");
    if (array.length >= 1) {
      sb.append(array[0]);
    }
    for (int i = 1; i < array.length; i++) {
      sb.append(",\t");
      sb.append(array[i]);
    }
    sb.append("]");
    return sb.toString();
  }

  static void help() {
    System.out.printf("Usage: VectorAddition [--benchmark SAMPLES] THREADS LENGTH\n");
    System.exit(1);
  }

  static Args get_args(String[] args) {
    if (!(args.length == 2 || args.length == 4)) {
      help();
    }

    boolean benchmark = args[0].equals("--benchmark");

    int num_samples = benchmark ? Integer.parseInt(args[1]) : -1;
    int args_start = benchmark ? 2 : 0;

    int num_threads = Integer.parseInt(args[args_start + 0]);
    int length = Integer.parseInt(args[args_start + 1]);

    if (num_samples == 0) {
      System.out.printf("SAMPLES must be greater than 0\n");
      System.exit(1);
    }

    if (num_threads <= 0) {
      System.out.printf("THREADS must be greater than 0\n");
      System.exit(1);
    }

    if (length <= 0) {
      System.out.printf("LENGTH must be greater than 0\n");
      System.exit(1);
    }

    return new Args(benchmark, num_samples, num_threads, length);
  }

  static void check_result(int[] vec1, int[] vec2, int[] result) {
    assert (vec1.length == vec2.length);
    assert (vec2.length == result.length);
    for (int i = 0; i < vec1.length; i++) {
      assert (vec1[i] + vec2[i] == result[i]);
    }
  }

  static Result do_addition(int num_threads, int length) {
    ThreadLocalRandom rng = ThreadLocalRandom.current();

    int[] vec1 = new int[length];
    int[] vec2 = new int[length];
    int[] result = new int[length];
    Timing[] timings = new Timing[num_threads];

    for (int i = 0; i < length; i++) {
      vec1[i] = rng.nextInt();
      vec2[i] = rng.nextInt();
    }

    int elems_per_thread = length / num_threads;

    Thread[] threads = new Thread[num_threads];
    int start_idx = 0;
    for (int id = 0; id < num_threads; id++) {
      int end_idx = id != (num_threads - 1) ? start_idx + elems_per_thread : length;
      threads[id] = new Thread(new Adder(id, timings, vec1, vec2, result, start_idx, end_idx));
      start_idx = end_idx;
    }

    long start_time = System.nanoTime();

    for (Thread t : threads) {
      t.start();
    }

    for (Thread t : threads) {
      try {
        t.join();
      } catch (InterruptedException e) {
        System.err.println(e);
        System.exit(1);
      }
    }

    long end_time = System.nanoTime();
    long elapsed = end_time - start_time;
    double freq = 1.0e9 / (double) elapsed;
    return new Result(elapsed, freq, timings);
  }

  static void benchmark(int num_samples, int num_threads, int length) {
    double[] frequencies = new double[num_samples];
    double mean = 0.0;

    // System.err.printf("\n");
    for (int i = 0; i < num_samples; i++) {
      Result result = do_addition(num_threads, length);
      // System.out.printf("%d\t%f\n", num_threads,result.freq_hz);
      frequencies[i] = result.freq_hz;
      mean += result.freq_hz;
    }
    mean /= num_samples;

    double variance = 0.0;
    for (double freq : frequencies) {
      double diff = freq - mean;
      variance += diff * diff;
    }
    variance /= num_samples;

    double std_deviation = Math.sqrt(variance);

    System.out.printf("%d\t%f\t%f\n", num_threads, mean, std_deviation);
  }

  public static void main(String[] argv) {
    Args args = get_args(argv);

    if (args.benchmark) {
      benchmark(args.num_samples, args.num_threads, args.length);
    } else {
      System.out.printf("num_threads:\t%d\nlength:\t\t%d\n", args.num_threads, args.length);
      Result result = do_addition(args.num_threads, args.length);
      System.out.printf("Time (ns): %d\n", result.time_nanoseconds);
      System.out.printf("Freq (Hz): %f\n", result.freq_hz);

      if (true) {
        Timing[] timings = result.timings;
        // print the relative start and end times of each thread
        System.out.printf("Thread timings:\n");
        Arrays.sort(timings);
        long base = timings[0].start;
        for (Timing t : timings) {
          System.out.printf("%d\t%d\t%d\n", t.id + 1, t.start - base, t.end - base);
        }
      }
    }
  }
}
